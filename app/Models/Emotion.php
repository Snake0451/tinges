<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Emotion extends Model
{
    public function videos() {
        return $this->belongsToMany( 'App\Models\Video');
    }

    public function reactions() {
        return $this->hasMany( 'App\Models\Reaction');
    }
}
