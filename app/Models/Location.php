<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    public $fillable = [
        'lng', 'lat', 'name', 'countrycode'
    ];

    public function videos () {
        return $this->hasMany ('App\Models\Video');
    }
}
