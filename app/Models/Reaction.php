<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reaction extends Model
{
    public $fillable = [
        'video_id', 'type_id'
    ];

    public function video() {
        return $this->belongsTo( 'App\Models\Video');
    }

    public function reaction_type() {
        return $this->belongsTo( 'App\Models\ReactionType');
    }
}
