<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{

    protected $fillable = ['title'];

    public function videos () 
    {
        return $this->hasMany ( 'App\Models\Video' )
            ->whereNotNull('latitude')
            ->whereNotNull('longitude');
    }

    public function allVideos () 
    {
        return $this->hasMany ( 'App\Models\Video' )
            ->where();
    }

    public function firstVideo() 
    {
        return $this->videos()
            ->selectRaw('videos.youtube_id')
            ->oldest()
            ->limit(1);
    }

    public function reactions ()
    {
        return $this->hasManyThrough ( 'App\Models\Reaction', 'App\Models\Video' );
    }

    public function emotions ()
    {
        return $this->hasMany ( 'App\Models\Video' )
            ->selectRaw ( 'count(emotion_video.id) as videos_count' ) 
            ->selectRaw ( 'emotions.*' )
            ->join ( 'emotion_video', 'emotion_video.video_id', '=', 'videos.id' )
            ->join ( 'emotions', 'emotion_video.emotion_id', '=', 'emotions.id' )
            ->join ( 'events', 'videos.event_id', '=', 'events.id' )
            ->where ( 'events.id', $this->id )
            ->groupBy ( 'emotions.id' )
            ->groupBy ( 'emotions.name' ) 
            ->groupBy ( 'emotions.color' ) 
            ->groupBy ( 'emotions.created_at' ) 
            ->groupBy ( 'emotions.updated_at' );
    }
}
