<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReactionType extends Model
{
    public $table = 'reaction_types';

    public function reactions ()
    {
        return $this->hasMany( 'App\Models\Reaction');
    }

    public function videos ()
    {
        return $this->belongsToMany( 'App\Models\Video', 'reactions', 'type_id', 'video_id');
    }
}
