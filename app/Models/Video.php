<?php

namespace App\Models;

use App\Models\Location;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Video extends Model
{
    protected $guarded = [];

    public function event()
    {
        return $this->belongsTo('App\Models\Event');
    }

    public function emotions()
    {
        return $this->belongsToMany('App\Models\Emotion');
    }

    public function reactions()
    {
        return $this->hasMany('App\Models\Reaction');
    }

    public function location()
    {
        return $this->belongsTo('App\Models\Location');
    }

    public function reactionTypes()
    {
        return $this->belongsToMany('App\Models\ReactionType', 'reactions', 'video_id', 'type_id');
    }

    public function reactionTypesCount()
    {
        return $this->belongsToMany('App\Models\ReactionType', 'reactions', 'video_id', 'type_id')
            ->selectRaw('reaction_types.id')
            ->selectRaw('reaction_types.name')
            ->selectRaw('count(reactions.type_id) as count')
            ->groupBy('reactions.video_id')
            ->groupBy('reactions.type_id')
            ->groupBy('reaction_types.id')
            ->groupBy('reaction_types.name');
    }

    public function fillVideo()
    {
        $this->reactions = json_decode(json_encode($this->reactionTypesCount), false);

        $this->reactions = new \stdClass();
        foreach ($this->reactionTypesCount as $value) {
            $key = $value->name;
            $this->reactions->$key = $value;
        }
        return $this;
    }
}
