<?php

namespace App\Services;

use App\Models\Video;
use App\Models\Emotion;
use App\Models\Event;
use Illuminate\Support\Facades\DB;

class EventService 
{
    public function index ( $orderBy, $params ) 
    {
        if ( isset( json_decode( $params, true )[ 'emotions' ] ) && json_decode( $params, true )[ 'emotions' ] === [] ) { 
            return null;
        }
        
        $events = Event::has( 'videos' )->withCount( 'videos' )->withCount( 'reactions' )
        ->paginate(8);

        $emotion_array = isset( json_decode( $params, true )[ 'emotions' ] ) ? json_decode( $params, true )[ 'emotions' ] : null;

        if ( $emotion_array ) {
            foreach ( $events as $event ){
                // $videoQuery = Video::whereNotNull('longitude')
                // ->whereNotNull('latitude')
                $videoQuery = $event->videos()                
                ->whereHas ( 'emotions', function ( $emotionQuery ) use ( $emotion_array ) {  
                    $emotionQuery->whereIn( 'emotions.id', $emotion_array );
                });
                //dd($videoQuery->get());
                $event->videos_count = $videoQuery
                    ->groupBy('event_id')
                    ->count('id');
                $event->views = intval( $videoQuery
                    ->groupBy('event_id')
                    ->sum('views') );
                $event->emotions = $event->emotions()
                    ->whereIn('emotions.id', $emotion_array)
                    ->get();
                $event->firstVideo = $event->firstVideo()
                    ->whereHas ( 'emotions', function ( $emotionQuery ) use ( $emotion_array ) {  
                        $emotionQuery->whereIn( 'emotions.id', $emotion_array );
                    })
                    ->first();
                } 
                $events = $events->filter(function ($event) {
                    return $event->videos_count > 0;
                });
            }
            else {
                foreach ( $events as $event ){
                    $event->views = DB::table('videos')
                        ->where('event_id', $event->id)
                        ->groupBy('event_id')
                        ->sum('views');
                        $event->time_published = DB::table('videos')
                    ->where('event_id', $event->id)
                    ->min('time_published');
                $event->emotions;
                $event->firstVideo;
                }
            
        }

        switch ( $orderBy ) {
            case 'time_published':
                $events = $events->sortByDesc ('time_published')->values();
                break;
            case 'time_published_asc':
                $events = $events->sortBy ('time_published')->values();
                break;
            case 'views':
                $events = $events->sortByDesc ('views')->values();
                break;
            case 'views_asc':
                $events = $events->sortBy ('views')->values();
                break;
            default:
        }

        return $events;
    }
} 