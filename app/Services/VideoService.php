<?php

namespace App\Services;

use App\Models\Emotion;
use App\Models\Event;
use App\Models\Video;

class VideoService
{
    public function playlistPaginated($params, $orderBy)
    {

        if (isset(json_decode($params, true)['emotions']) && json_decode($params, true)['emotions'] === []) {
            return null;
        }

        $query = $this->filter($params, Video::withCount('reactions')
                ->with('reactionTypesCount')
                ->with('location')
                ->with('emotions')
                ->whereNotNull('latitude')
                ->whereNotNull('longitude'));

        switch ($orderBy) {
            case 'time_published':
                $query->orderBy('time_published', 'desc');
                break;
            case 'time_published_asc':
                $query->orderBy('time_published', 'asc');
                break;
            case 'views':
                $query->orderBy('views', 'desc');
                break;
            case 'views_asc':
                $query->orderBy('views', 'asc');
                break;
            default:
                $query->orderBy('id', 'asc');
        }

        foreach ($videos = $query->paginate(8) as $video) {
            $video->fillVideo();
        }

        return $videos;
    }

    public function filter($params, $query)
    {
        if (isset(json_decode($params, true)['emotions']) && json_decode($params, true)['emotions'] === []) {
            return null;
        }

        $event_array = isset(json_decode($params, true)['events']) ? json_decode($params, true)['events'] : null;
        $emotion_array = isset(json_decode($params, true)['emotions']) ? json_decode($params, true)['emotions'] : null;
        if ($event_array) {
            if ($event_array === ["all"]) {
                $event_array = Event::pluck('id');
            }
        }
        if ($emotion_array) {
            $query->whereHas('emotions', function ($emotionQuery) use ($emotion_array) {
                $emotionQuery->whereIn('emotions.id', $emotion_array);
            });
        }

        if ($event_array) {
            $query->whereIn('event_id', $event_array);
        }

        return $query;
    }

    public function playlist($params, $orderBy)
    {

        if (isset(json_decode($params, true)['emotions']) && json_decode($params, true)['emotions'] === []) {
            return null;
        }

        $query = $this->filter($params, Video::withCount('reactions')
                ->with('reactionTypesCount')
                ->with('location')
                ->with('emotions')
                ->whereNotNull('latitude')
                ->whereNotNull('longitude'));

        switch ($orderBy) {
            case 'time_published':
                $query->orderBy('time_published', 'desc');
                break;
            case 'time_published_asc':
                $query->orderBy('time_published', 'asc');
                break;
            case 'views':
                $query->orderBy('views', 'desc');
                break;
            case 'views_asc':
                $query->orderBy('views', 'asc');
                break;
            default:
                $query->orderBy('id', 'asc');
        }

        foreach ($videos = $query->get() as $video) {
            $video->fillVideo();
        }

        return $videos;
    }

    public function playlistInfo($params)
    {
        $info = [];
        $emotion_array = isset(json_decode($params, true)['emotions']) ? json_decode($params, true)['emotions'] : null;
        $event_array = isset(json_decode($params, true)['events']) ? json_decode($params, true)['events'] : null;

        if (isset(json_decode($params, true)['emotions'])) {
            $info = array_merge($info, ['emotions' => Emotion::whereIn('id', $emotion_array)->get()]);
        }

        if (isset(json_decode($params, true)['events'])) {
            $info = array_merge($info, ['events' => Event::whereIn('id', $event_array)->get()]);
        }

        return $info;
    }

    public function videosInBox($north, $south, $east, $west, $orderBy, $params)
    {
        if (isset(json_decode($params, true)['emotions']) && json_decode($params, true)['emotions'] === []) {
            return null;
        }

        $query = $this->filter($params, Video::withCount('reactions')
                ->with('reactionTypesCount')
                ->with('location')
                ->with('emotions')
                ->whereNotNull('latitude')
                ->whereNotNull('longitude'));

        if ($west > $east) {
            $query->whereBetween('longitude', [$west, 180])
                ->whereRaw('longitude BETWEEN ? AND ?', [-180, $east])
                ->whereBetween('latitude', [$south, $north]);
        } else {
            $query->whereBetween('longitude', [$west, $east])
                ->whereBetween('latitude', [$south, $north]);
        }

        if (isset($params)) {
            $query = $this->filter($params, $query);
        }

        switch ($orderBy) {
            case 'time_published':
                $query->orderBy('time_published', 'desc');
                break;
            case 'time_published_asc':
                $query->orderBy('time_published', 'asc');
                break;
            case 'views':
                $query->orderBy('views', 'desc');
                break;
            case 'views_asc':
                $query->orderBy('views', 'asc');
                break;
            default:
                $query->orderBy('id', 'asc');
        }

        $videos = $query->paginate(8);

        foreach ($videos as $video) {
            $video->fillVideo();
        }

        return $videos;
    }

    public function addShare(Video $video)
    {
        $video->shares += 1;
        $video->save();
        return $video->shares;
    }

    public function addComment(Video $video)
    {
        $video->comments += 1;
        $video->save();
        return $video->comments;
    }

    public function getVideo($youtubeId, $video)
    {
        try
        {
            $client = new \Google_Client();
            $client->setDeveloperKey(env('YOUTUBE_API_KEY'));
            $youtube = new \Google_Service_YouTube($client);
            $youtubeVideo = $youtube->videos->listVideos('snippet, recordingDetails, statistics, contentDetails', array('id' => $youtubeId));

            if (!count($youtubeVideo['items'])) {
                return 0;
            }

            $youtubeVideo = $youtubeVideo['items'][0];

            $video->youtube_id = $youtubeId;
            $video->title = $youtubeVideo['snippet']['title'];
            $video->description = $youtubeVideo['snippet']['description'];

            $video->latitude = $youtubeVideo['recordingDetails']['location']['latitude'];
            $video->longitude = $youtubeVideo['recordingDetails']['location']['longitude'];

            $video->author = $youtubeVideo['snippet']['channelTitle'];

            $video->time_published = Carbon::parse($youtubeVideo['snippet']['publishedAt']);

            $video->thumbnail_url = $youtubeVideo['snippet']['thumbnails']['medium']['url'];

            $video->views = $youtubeVideo['statistics']['viewCount'];
            $video->duration = $youtubeVideo['contentDetails']['duration'];

            if (isset($video->latitude) && isset($video->longitude)) {
                $video->location_id = Location::orderBy(DB::raw('sqrt((\'locations.lat\' - ' . $video->latitude . ') * (locations.lat - ' . $video->latitude . ') + (locations.lng - ' . $video->longitude . ') * (locations.lng - ' . $video->longitude . '))'), 'asc')
                    ->first()->id;
            }
            return $video;

            return $video;
        } catch (Exception $e) {
            return $e->getCode();
        }
    }
}
