<?php

namespace App\Services;

use App\Models\Video;
use App\Models\Reaction;
use App\Models\ReactionType;
use App\Http\Requests\AddReactionRequest;
use Mockery\CountValidator\Exception;

class ReactionService 
{
    public function add ( $args ) 
    {
        Reaction::create ( $args );
        $video = Video::withCount( 'reactions' )->find( $args[ 'video_id' ] )->fillVideo();
        $response = $video->reactions;
        $response->reactions_count = $video->reactions_count;
        return $response;
    }

    public function delete ( $args )
    {
        $reaction = Reaction::where( 'video_id', $args[ 'video_id' ] )
            ->where( 'type_id', $args[ 'type_id' ] )
            ->first();

        if( isset( $reaction ) ) {
            $reaction->delete();
        }

        $video = Video::withCount( 'reactions' )->find( $args[ 'video_id' ] )->fillVideo();
        $response = $video->reactions;
        $response->reactions_count = $video->reactions_count;
        return $response;
    }
} 