<?php

namespace App\Http\Middleware;

use Closure;

class Facebook
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd(strpos($request->header('user-agent'), 'facebookexternalhit'));
        if( strpos($request->header('user-agent'), 'facebookexternalhit' ) !== false || 
            strpos($request->header('user-agent'), 'developers.google.com' ) !== false ||
            strpos($request->header('user-agent'), 'Twitterbot' ) !== false ||
            strpos($request->header('user-agent'), 'vkShare' ) !== false ) {
            // dd($request->url());
            $tokens = explode( '/', $request->url() );
            return redirect('/fakepage/' . end( $tokens ) );
        }
        return $next($request);
    }
}
