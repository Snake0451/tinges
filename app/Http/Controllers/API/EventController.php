<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Services\EventService;

class EventController extends Controller
{
    private $eventService;

    public function __construct(EventService $eventService)
    {
        $this->eventService = $eventService;
    }

    /**
     * Return a listing of all emotions.
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Get(
     *     path="/api/events",
     *     description="Returns list of all events.",
     *     operationId="api.events.index",
     *     produces={"application/json"},
     *     tags={"events"},
     *     @SWG\Response(
     *         response=200,
     *         description="Returns list of events."
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized events.",
     *     )
     * )
     */

    public function index ( Request $request )
    {
        return response()->json ( $this->eventService->index( $request->get( 'order_by' ), $request->get( 'params' ) ) );
    }
}
