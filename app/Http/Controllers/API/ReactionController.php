<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Reaction;
use App\Models\ReactionType;
use App\Http\Requests\AddReactionRequest;
use App\Services\ReactionService;

class ReactionController extends Controller
{
    public function __construct( ReactionService $reactionService )
    {
        $this->reactionService = $reactionService;
    }

    /**
     * Add reaction to video.
     *
     * @return \Illuminate\Http\JsonResponse 
     *
     * @SWG\Post(
     *     path="/api/reaction",
     *     description="Add reaction to video",
     *     operationId="api.reaction.add",
     *     tags={"reaction"},
     *     @SWG\Parameter(
     *          name="video_id",
     *          in="formData",
     *          required=true,
     *          type="number"
     *     ),
     *     @SWG\Parameter(
     *          name="type_id",
     *          in="formData",
     *          required=true,
     *          type="number"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Reaction has been added."
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized action.",
     *     )
     * )
     */

    public function addReaction ( AddReactionRequest $request ) 
    {
        return response ()->json ( $this->reactionService->add ( $request->all() ) );
    }

    /**
     * Delete reaction from video.
     *
     * @return \Illuminate\Http\JsonResponse 
     *
     * @SWG\Delete(
     *     path="/api/reaction",
     *     description="Delete reaction from video",
     *     operationId="api.reaction.delete",
     *     tags={"reaction"},
     *     @SWG\Parameter(
     *          name="video_id",
     *          in="formData",
     *          required=true,
     *          type="number"
     *     ),
     *     @SWG\Parameter(
     *          name="type_id",
     *          in="formData",
     *          required=true,
     *          type="number"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Reaction has been deleted."
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized deleted.",
     *     )
     * )
     */    

    public function deleteReaction ( AddReactionRequest $request ) 
    {
        return response ()->json ( $this->reactionService->delete ( $request->all() ) );
    }
}
