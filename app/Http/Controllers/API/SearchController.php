<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Models\Location;
use App\Models\Video;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    public function autocomplete ( Request $request ) {
        $query = $request->get ( 'query' );
        return response()->json ( [

            'videos' => Video::where( 'title', 'rlike', '^.*' . $query . '.*$')
            ->whereNotNull('longitude')
            ->whereNotNull('latitude')
            ->orderBy('views', 'desc')
            ->limit(5)
            ->get(),

            'locations' => Location::withCount('videos')
            ->where( 'name', 'rlike', '^.*' . $query . '.*$')
            ->orderBy('videos_count', 'desc')
            ->limit(5)
            ->get() 

            ]); 
    }
}
