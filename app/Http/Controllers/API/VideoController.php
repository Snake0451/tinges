<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Emotion;
use App\Models\Video;
use App\Services\VideoService;
use Illuminate\Http\Request;

class VideoController extends Controller
{

    private $videoService;

    public function __construct(VideoService $videoService)
    {
        $this->videoService = $videoService;
    }

    /**
     * Display a listing of all videos.
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Get(
     *     path="/api/videos",
     *     description="Returns list of all videos.",
     *     operationId="api.video.index",
     *     produces={"application/json"},
     *     tags={"video"},
     *     @SWG\Response(
     *         response=200,
     *         description="Returns list of videos."
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized action.",
     *     )
     * )
     */

    public function index()
    {
        $videos = Video::withCount('reactions')
            ->with('reactionTypesCount')
            ->with('location')
            ->with('emotions')
            ->whereNotNull('latitude')
            ->whereNotNull('longitude')
            ->get();
        return response()->json($videos);
    }

    /**
     * Display a paginated listing of all videos.
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Get(
     *     path="/api/videos-paginated",
     *     description="Returns list of all videos paginated by 5.",
     *     operationId="api.video.indexPaginated",
     *     produces={"application/json"},
     *     tags={"video"},
     *     @SWG\Parameter(
     *          name="page",
     *          in="query",
     *          required=false,
     *          type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Returns list of videos paginated by 5."
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized action.",
     *     )
     * )
     */

    public function indexPaginated()
    {
        $videos = Video::withCount('reactions')
            ->with('reactionTypesCount')
            ->with('location')
            ->with('emotions')
            ->whereNotNull('latitude')
            ->whereNotNull('longitude')
            ->paginate(8);
        foreach ($videos as $video) {
            $video->fillVideo();
        }
        return response()->json($videos);
    }

    /**
     * Display a paginated listing of the videos that fits in the borders.
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Get(
     *     path="/api/inbox",
     *     description="Returns list of videos which location fits in the borders.",
     *     operationId="api.video.videosInBox",
     *     produces={"application/json"},
     *     tags={"video"},
     *     @SWG\Parameter(
     *          name="north",
     *          in="query",
     *          required=true,
     *          type="number"
     *     ),
     *     @SWG\Parameter(
     *          name="south",
     *          in="query",
     *          required=true,
     *          type="number"
     *     ),
     *     @SWG\Parameter(
     *          name="east",
     *          in="query",
     *          required=true,
     *          type="number"
     *     ),
     *     @SWG\Parameter(
     *          name="west",
     *          in="query",
     *          required=true,
     *          type="number"
     *     ),
     *     @SWG\Parameter(
     *          name="page",
     *          in="query",
     *          required=false,
     *          type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Returns list of videos inside the borders."
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized action.",
     *     )
     * )
     */

    public function videosInBox(Request $request)
    {
        return response()->json($this->videoService
                ->videosInBox($request->get('north'),
                    $request->get('south'),
                    $request->get('east'),
                    $request->get('west'),
                    $request->get('order_by'),
                    $request->get('params')));
    }

    /**
     * @SWG\Get(
     *     path="/api/video/{video_id}",
     *     description="Returns a video with specified ID.",
     *     operationId="api.video.show",
     *     produces={"application/json"},
     *     tags={"video"},
     *     @SWG\Parameter(
     *          name="video_id",
     *          in="path",
     *          required=true,
     *          type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Returns video."
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized action.",
     *     )
     * )
     */

    public function show($id)
    {
        $video = Video::withCount('reactions')
            ->with('reactionTypesCount')
            ->with('location')
            ->with('emotions')
            ->find($id);
        return response()->json($video->fillVideo());
    }

    /**
     * @SWG\Put(
     *     path="/api/video/{video}/add-share",
     *     description="Returns a video with specified ID.",
     *     operationId="api.video.addShare",
     *     produces={"application/json"},
     *     tags={"video"},
     *     @SWG\Parameter(
     *          name="video",
     *          in="path",
     *          required=true,
     *          type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Added one share to video."
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized action.",
     *     )
     * )
     */

    public function addShare(Video $video)
    {
        return response()->json($this->videoService->addShare($video));
    }

    /**
     * @SWG\Get(
     *      path="/api/playlist-paginated",
     *      description="Returns paginated collection of videos that matches events and/or emotions",
     *      operationId="api.video.playlist",
     *      produces={"application/json"},
     *      tags={"video"},
     *      @SWG\Parameter(
     *          name="params",
     *          description="JSON string with emotion ID's and/or event ID's.",
     *          in="query",
     *          required=false,
     *          type="string",
     *      ),
     *     @SWG\Parameter(
     *          name="page",
     *          in="query",
     *          required=false,
     *          type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Returns playlist."
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized action.",
     *     )
     * )
     */

    public function playlist(Request $request)
    {
        return response()->json($this->videoService->playlist($request->get('params'), $request->get('order_by')));
    }

    public function playlistPaginated(Request $request)
    {
        return response()->json($this->videoService->playlistPaginated($request->get('params'), $request->get('order_by')));
    }

    public function addComment(Video $video)
    {
        return response()->json($this->videoService->addComment($video));
    }
}
