<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Emotion;
use App\Models\Event;
use Illuminate\Http\Request;

class EmotionsController extends Controller
{
    /**
     * Return a listing of all emotions.
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Get(
     *     path="/api/emotions",
     *     description="Returns list of all emotions.",
     *     operationId="api.emotions.index",
     *     produces={"application/json"},
     *     tags={"emotions"},
     *     @SWG\Response(
     *         response=200,
     *         description="Returns list of emotions."
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized action.",
     *     )
     * )
     */

    public function index(Request $request)
    {
        if (isset(json_decode($request['params'], true)['events'])) {
            $event_array = json_decode($request['params'], true)['events'];
            if ($event_array === ["all"]) {
                $event_array = Event::pluck('id');
            }
            $emotions = Emotion::withCount(['videos' => function ($query) use ($event_array) {
                $query->whereNotNull('longitude')
                    ->whereNotNull('latitude')
                    ->whereIn('event_id', $event_array);
            }])->get();
            return response()->json($emotions);
        }
        $emotions = Emotion::withCount(['videos' => function ($query) {
            $query->whereNotNull('longitude')
                ->whereNotNull('latitude');
        }])->get();

        return response()->json($emotions);
    }
}
