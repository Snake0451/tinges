<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Video;

class FakePageController extends Controller
{
    public function fake ( $id ) 
    {
        //$url = session('url');
        //$video = null;
        //$tokens = explode('/', $url);
        //if($tokens[ count($tokens) - 2 ] === 'videoList') {
        $video = Video::find($id);
        //}
        //dd($video);
        return view('fake')->with('video', $video);
    }
}
