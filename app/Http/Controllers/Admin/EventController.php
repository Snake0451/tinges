<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddEventRequest;
use App\Models\Event;
use App\Models\Video;
use \Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Request;


class EventController extends Controller
{
    public function listEvents ()
    {
        $events = Event::with('videos')->get();
        foreach($events as $event)
        {
            $event->count = $event->videos->count();
        }
        if(isset($message))
            return view('admin.listEvents')->with(['events' => $events, 'message' => $message]);
        return view('admin.listEvents')->with('events', $events);
    }

    public function showEvent (Event $event)
    {
        $videos = $event->videos;

        if( isset($message) ){
            return view('admin.showEvent')->with(['event' => $event, 'message' => $message, 'videos' => $videos]);
        }
        return view('admin.showEvent')->with(['event' => $event, 'videos' => $videos]);
    }

    public function addEvent (AddEventRequest $request)
    {
        $validated = $request->validated();
        $input = $request->all();

        if(!isset($input['title']))
            return view('admin.addEvent')->with('message', 'Please, specify title of event.');
        try
        {
            Event::create($input);
        }
        catch(Exception $e)
        {
            return view('admin.addEvent')->with('message', $e->getMessage());
        }
        return redirect()->route('listEvents')->with('message', 'Event has been added');
    }

    public function deleteEvent (Event $event) 
    {
        $event->delete();
        return redirect()->route('listEvents')->with('message', 'Event has been added');
    }

    public function updateEvent ( Event $event, AddEventRequest $request )
    {
        $validated = $request->validated();
        $event->title = $request->get( 'title' );
        $event->save();
        return redirect()->route('listEvents')->with('message', 'Event has been added');
    }
}
