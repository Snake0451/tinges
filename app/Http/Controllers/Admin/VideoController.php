<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddVideoRequest;
use App\Models\Emotion;
use App\Models\Event;
use App\Models\Location;
use App\Models\Video;
use App\Services\VideoService;
use Illuminate\Http\Request;
use \Illuminate\Database\QueryException;

class VideoController extends Controller
{

    private $videoService;

    public function __construct(VideoService $videoService)
    {
        $this->videoService = $videoService;
    }

    /**
     * @param App\Http\Requests\AddVideoRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */

    public function addVideo(AddVideoRequest $request)
    {
        $validated = $request->validated();
        $input = $request->all();

        $video = new Video;
        $video = $this->videoService->getVideo($input['id'], $video);

        if ($input['event_id'] == -1) {
            $video->event_id = null;
        } else {
            $video->event_id = $input['event_id'];
        }

        //$video->emotion_id = $input['emotion_id'];
        try
        {
            $video->save();
            foreach ($input['emotions'] as $emotion) {
                $video->emotions()->attach($emotion);
            }
        } catch (QueryException $e) {
            return redirect()->back()->with('message', $e->getMessage());
        }

        if ($video->latitude == null || $video->longitude == null) {
            return redirect('admin/video/' . $video->id)->with('message', 'Video doesn\'t has geolocation. Please, specify it, if you want it to be visible on map!');
        }

        return redirect('admin/video/' . $video->id)->with('message', 'Video has been added!');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listVideos()
    {
        $videos = Video::with('emotions')
            ->with('event')
            ->paginate(10);

        $events = Event::all();
        $emotions = Emotion::all();

        $event_list = array(-1 => 'No event');
        $emotion_list = array();
        foreach ($events as $event) {
            $event_list = array_add($event_list, $event->id, $event->title);
        }

        foreach ($emotions as $emotion) {
            $emotion_list = array_add($emotion_list, $emotion->id, $emotion->name);
        }

        if (session()->has('message')) {
            return view('admin.listVideos')
                ->with('videos', $videos)
                ->with('event_list', $event_list)
                ->with('emotion_list', $emotion_list)
                ->with('message', session('message'));
        }

        return view('admin.listVideos')
            ->with('videos', $videos)
            ->with('event_list', $event_list)
            ->with('emotion_list', $emotion_list);
    }

    /**
     * @param App\Models\Video $video
     * @param Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateVideo(Video $video, Request $request)
    {
        //$validated = $request->validated();
        $input = $request->all();

        $video->title = $input['title'];

        if (isset($input['description']) && $input['description'] != '') {
            $video->description = $input['description'];
        }

        if (isset($input['latitude']) && $input['latitude'] != '') {
            $video->latitude = $input['latitude'];
        }

        if (isset($input['event_id']) && $input['event_id'] != '') {
            if ($input['event_id'] == 'No event') {
                $video->event_id = null;
            } else {
                $video->event_id = $input['event_id'];
            }
        }

        if (isset($input['longitude']) && $input['longitude'] != '') {
            $video->longitude = $input['longitude'];
        }

        $video->save();

        $video->emotions()->detach();

        if (isset($input['emotions'])) {
            foreach ($input['emotions'] as $emotion) {
                $video->emotions()->attach($emotion);
            }
        }

        return redirect('/admin/video/')->with('message', 'Video has been updated');
    }

    /**
     * @param App\Models\Video $video
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showVideo(Video $video)
    {
        $event_list = [];
        $emotions_list = [];
        $checked_emotions = [];

        if (!$video) {
            return abort(404);
        }

        $video->emotions;
        $video->reactionTypesCount;
        $video->location;
        $video->fillVideo();

        if (isset($video->event_id)) {
            $event_list = array_add(array(), $video->event_id, Event::find($video->event_id)->title);
            $event_list = array_add($event_list, 'No event', 'No event');
            foreach (Event::all()->except($video->event_id) as $event) {
                $event_list = array_add($event_list, $event->id, $event->title);
            }
        } else {
            $event_list = array_add(array(), 'No event', 'No event');
            foreach (Event::all() as $event) {
                $event_list = array_add($event_list, $event->id, $event->title);
            }
        }

        foreach (Emotion::all() as $emotion) {
            $emotions_list = array_add($emotions_list, $emotion->id, $emotion->name);
        }

        foreach ($video->emotions as $key => $emotion) {
            $checked_emotions[$key] = $emotion->id;
        }

        if (session()->has('message')) {
            return view('admin.showVideo')
                ->with('video', $video)
                ->with('event_list', $event_list)
                ->with('emotion_list', $emotions_list)
                ->with('checked_emotions', $checked_emotions)
                ->with('message', session('message'));
        }

        return view('admin.showVideo')
            ->with('video', $video)
            ->with('event_list', $event_list)
            ->with('emotion_list', $emotions_list)
            ->with('checked_emotions', $checked_emotions);
    }

    /**
     * @param App\Models\Video $video
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteVideo(Video $video)
    {
        $video->delete();
        return redirect('/admin/video/')->with('message', 'Video ' . $video->name . ' has been deleted.');
    }
}
