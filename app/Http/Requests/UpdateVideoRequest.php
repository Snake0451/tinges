<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateVideoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => [ 'required' ],
            'latitude' => [ 'between:-180,180|numeric' ],
            'longitude' => [ 'between-180,180|numeric' ],
            'event_id' => [ 'exists:events,id' ],
            'emotions'=> [ 'array|exists:emotions,id' ]
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Please, specify title!',
            'emotion_id.required' => 'Please, specify emotion!',
            'event_id' => 'Please, specify event!',

        ];
    }    
}
