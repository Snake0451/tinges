<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Video;

class ClearDead extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'videos:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Search videos witch was deleted from Youtube and deletes them from DB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new \Google_Client();
        $client->setDeveloperKey('AIzaSyCjptbERqG3kcBjppdA1zaYL6aGHLLsweA');
        $youtube = new \Google_Service_YouTube($client);

        $idChunks = array_chunk(Video::pluck('youtube_id')->toArray(), 50);
        //$ids = implode(',', Video::pluck('youtube_id')->toArray());
        $toDelete = [];
        foreach($idChunks as $ids){
            $youtubeVideos = $youtube->videos->listVideos('id', ['id' => implode(',', $ids)] )['items'];
            if ( count($youtubeVideos) < count($ids) ) {
                $youtubeIdArray = [];
                foreach ($youtubeVideos as $video) {
                    array_push($youtubeIdArray, $video['id']);
                    //var_dump($video['id']);
                }
                $toDelete = array_merge($toDelete, array_diff( $ids, $youtubeIdArray ));
            }
        }
        Video::whereIn('youtube_id', $toDelete)->delete();
        echo count($toDelete) . ' missing videos has been deleted: ';
        foreach($toDelete as $str) {
            echo $str . ', ';
        } 
    }
}
