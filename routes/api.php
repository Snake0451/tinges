<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/videos', 'API\VideoController@index');

Route::get('/videos-paginated', 'API\VideoController@indexPaginated');

Route::get('/video/{video}', 'API\VideoController@show');

Route::get('/video/{video}/add-share', 'API\VideoController@addShare');

Route::get('/video/{video}/add-comment', 'API\VideoController@addComment');

Route::get('/playlist', 'API\VideoController@playlist');

Route::get('/playlist-paginated', 'API\VideoController@playlistPaginated');

Route::get('/inbox', 'API\VideoController@videosInBox');

Route::get('/emotions', 'API\EmotionsController@index');

Route::get('/events', 'API\EventController@index');

Route::get('/autocomplete', 'API\SearchController@autocomplete');

Route::post('/reaction', 'API\ReactionController@addReaction');

Route::delete('/reaction', 'API\ReactionController@deleteReaction');

Route::get('/comments', 'Admin\VideoController@sendReviewsRequest');