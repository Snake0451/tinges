<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return File::get(public_path('/assets/index.html'));
})
    ->middleware('facebook');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::middleware('auth.basic')->prefix('/admin')->group(function () {
Route::get('/', function(){
        return view('admin.dashboard');
    })->name('dashboard');
    Route::group(['prefix'=>'/video'], function (){
        Route::get('/', 'Admin\VideoController@listVideos')->name('listVideos');
        Route::get('/{video}', 'Admin\VideoController@showVideo')->name('showVideo');
        Route::post('/', 'Admin\VideoController@addVideo')->name('addVideo');
        Route::put('/{video}', 'Admin\VideoController@updateVideo')->name('showVideo');
        Route::delete('/{video}', 'Admin\VideoController@deleteVideo')->name('deleteVideo');
    });

    Route::group(['prefix' => '/event'], function () {
        Route::get('/', 'Admin\EventController@listEvents')->name('listEvents');
        Route::get('/{event}', 'Admin\EventController@showEvent')->name('showEvent');
        Route::post('/', 'Admin\EventController@addEvent')->name('addEvent');
        Route::put('/{event}', 'Admin\EventController@updateEvent')->name('updateEvent');
        Route::delete('/{event}', 'Admin\EventController@deleteEvent')->name('deleteEvent');
        });
});

Route::get('/fakepage/{id}', 'FakePageController@fake');

Route::any('/{uri?}', function () {
    return File::get(public_path('/assets/index.html'));
})
    ->where('uri', '^((?!admin).)*$')
    ->name('main')
    ->middleware('facebook');