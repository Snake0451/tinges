<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        Schema::create('videos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('youtube_id', 30)->unique();
            $table->string('title')->nullable()->default(null);
            $table->text('description')->nullable()->default(null);
            $table->datetime('time_published')->nullable();
            $table->integer('shares')->unsigned()->nullable();
            $table->integer('event_id')->unsigned()->nullable()->default(null);
            $table->foreign('event_id')->references('id')->on('events')->onDelete('set null')->onUpdate('cascade');
            $table->float('latitude', 10, 7)->nullable()->default(null);
            $table->float('longitude', 10, 7)->nullable()->default(null);
            $table->string('author');
            $table->string('authors_avatar')->nullable();
            $table->timestamps();
            $table->string('thumbnail_url')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
