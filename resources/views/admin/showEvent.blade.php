@extends('adminlte::page')
@section('content')
    @if(isset($message))
        <h3>{{$message}}</h3>
    @endif
    
    <table>
            @foreach($videos as $video)
                <tr>
                    <td><a href="{{'/admin/video/' . $video->id}}">{{ $video->youtube_id }}</a></td>
                    <td>{{ $video->title }}</td>
                    </form>
                </tr>
            @endforeach
    </table>
        <form action="{{URL::current()}}" method = "POST"))>
            <input type="hidden" name="_method" value="put" />
            <div class="form-group">
                <label for="title">Event title:</label>
                <input name="title" value="{{$event->title}}">
            </div>
            <button type="submit" class="btn btn-primary form-control">Update event</button>        <br>
        </form>
        <form method="POST" action="{{ URL::current() }}">
            <input type="hidden" name="_method" value="delete" />
            @csrf
            <button type="submit" class="btn btn-block btn-danger">Delete event</button>
        </form>
    </div>

@endsection