@extends('adminlte::page') 
@section('content') 
@if(isset($message))
    <h3>{{$message}}</h3>
@endif
@if ($errors->any())
    <h2>
    {{ implode('', $errors->all(':message')) }}
    </h2>
@endif    
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Events</h3>
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th>Name</th>
                <th>Color</th>
            </tr>
            @foreach($emotions as $emotion)
                <tr>
                    <td>{{ $emotion->name }}</td>
                    <td>
                        <form method="post" action="{{URL::current() . '/' . $emotion->id}}">
                            <input type="hidden" name="_method" value="put" />
                            <td>
                                <input type="text" name="color" value="{{$emotion->color}}">
                            </td>
                            @csrf
                            <td>
                                <button type="submit" class="btn btn-block btn-primary">Change color</button>
                            </td>
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
@endsection